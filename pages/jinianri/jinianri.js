// 在页面中定义插屏广告
let interstitialAd = null
Page({

  /**
   * 页面的初始数据
   */
  data: {
    days:0,
    settingDate:"点我设置",
  },

  //设置picker的函数
  bindDateChange: function(e){
    this.setData({
      settingDate: e.detail.value
    });
    var todayParse = Date.parse(new Date()) / 1000 / 24 / 3600;
    var settingDateParse = Date.parse(new Date(this.data.settingDate)) / 1000 / 24 / 3600;
    var day = Math.ceil((todayParse - settingDateParse));
    this.setData({
      days: day
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 在页面onLoad回调事件中创建插屏广告实例
    if (wx.createInterstitialAd) {
      interstitialAd = wx.createInterstitialAd({
        adUnitId: 'adunit-f4c1312e49000164'
      })
      interstitialAd.onLoad(() => {})
      interstitialAd.onError((err) => {})
      interstitialAd.onClose(() => {})
    }
  },
  onShow:function(){
     // 在适合的场景显示插屏广告
    if (interstitialAd) {
      interstitialAd.show().catch((err) => {
        console.error(err)
      })
    }
  },
 

  /**
   * 用户点击右上角分享
   */
  //分享
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    var path = '/pages/jinianri/jinianri?id=1';
    return {
      title: '算算今天是你穿上军装的第几天？',
      path: path,
      success: function (res) {
        // 转发成功
      },
      fail: function (res) {
        // 转发失败
      }
    }
  }
})